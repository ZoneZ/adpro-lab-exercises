package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OnionTest {
    private Veggies onionClass;

    @Before
    public void setUp() {
        onionClass = new Onion();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Onion", onionClass.toString());
    }
}
