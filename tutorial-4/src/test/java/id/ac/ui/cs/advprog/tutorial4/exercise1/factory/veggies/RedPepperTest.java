package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedPepperTest {
    private Veggies redPepperClass;

    @Before
    public void setUp() {
        redPepperClass = new RedPepper();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Red Pepper", redPepperClass.toString());
    }
}
