package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EggplantTest {
    private Veggies eggplantClass;

    @Before
    public void setUp() {
        eggplantClass = new Eggplant();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Eggplant", eggplantClass.toString());
    }
}
