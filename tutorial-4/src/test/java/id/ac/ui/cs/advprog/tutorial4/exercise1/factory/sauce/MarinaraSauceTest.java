package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class MarinaraSauceTest {

    private Class<?> marinaraSauceClass;

    @Before
    public void setUp() throws Exception {
        marinaraSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
    }

    @Test
    public void testMarinaraSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(marinaraSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testMarinaraSauceOverridetoStringMethod() throws Exception {
        Method toString = marinaraSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Sauce concreteMarinaraSauce = new MarinaraSauce();
        assertEquals("Marinara Sauce", concreteMarinaraSauce.toString());
    }
}
