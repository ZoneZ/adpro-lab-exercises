package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class ReggianoCheeseTest {

    private Class<?> reggianoCheeseClass;

    @Before
    public void setUp() throws Exception {
        reggianoCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese");
    }

    @Test
    public void testReggianoCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(reggianoCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testReggianoCheeseOverridetoStringMethod() throws Exception {
        Method toString = reggianoCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Cheese concreteReggianoCheese = new ReggianoCheese();
        assertEquals("Reggiano Cheese", concreteReggianoCheese.toString());
    }
}