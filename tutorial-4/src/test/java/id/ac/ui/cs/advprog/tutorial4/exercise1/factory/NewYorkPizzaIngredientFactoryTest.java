package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory nyFactory;

    @Before
    public void setUp() throws Exception {
        nyFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testCreateDough() {
        ThinCrustDough thinCrustDough = new ThinCrustDough();
        assertEquals(thinCrustDough.getClass(), nyFactory.createDough().getClass());
    }

    @Test
    public void testCreateSauce() {
        MarinaraSauce marinaraSauce = new MarinaraSauce();
        assertEquals(marinaraSauce.getClass(), nyFactory.createSauce().getClass());
    }

    @Test
    public void testCreateCheese() {
        ReggianoCheese reggianoCheese = new ReggianoCheese();
        assertEquals(reggianoCheese.getClass(), nyFactory.createCheese().getClass());
    }

    @Test
    public void testCreateVeggies() {
        Veggies[] expectedVeggies = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        Veggies[] producedVeggies = nyFactory.createVeggies();
        for (int i = 0; i < expectedVeggies.length; i++) {
            assertEquals(expectedVeggies[i].toString(), producedVeggies[i].toString());
        }
    }

    @Test
    public void testCreateClam() {
        FreshClams freshClams = new FreshClams();
        assertEquals(freshClams.getClass(), nyFactory.createClam().getClass());
    }
}