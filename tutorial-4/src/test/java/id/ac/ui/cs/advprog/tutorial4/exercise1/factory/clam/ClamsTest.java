package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ClamsTest {

    private Class<?> clamsInterfaceClass;

    @Before
    public void setUp() throws Exception {
        clamsInterfaceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = clamsInterfaceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testClamsHasToStringMethod() throws Exception {
        Method toString = clamsInterfaceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
