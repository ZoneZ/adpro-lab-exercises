package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.EpicCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.EpicClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.EpicDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.EpicSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory depokFactory;

    @Before
    public void setUp() throws Exception {
        depokFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testcreateDough() {
        EpicDough epicDough = new EpicDough();
        assertEquals(epicDough.getClass(), depokFactory.createDough().getClass());
    }

    @Test
    public void testcreateSauce() {
        EpicSauce epicSauce = new EpicSauce();
        assertEquals(epicSauce.getClass(), depokFactory.createSauce().getClass());
    }

    @Test
    public void testcreateCheese() {
        EpicCheese epicCheese = new EpicCheese();
        assertEquals(epicCheese.getClass(), depokFactory.createCheese().getClass());
    }

    @Test
    public void testcreateVeggies() {
        Veggies[] expectedVeggies = {new EpicVegetable(), new BlackOlives(), new Eggplant(), new Spinach()};
        Veggies[] producedVeggies = depokFactory.createVeggies();
        for (int i = 0; i < expectedVeggies.length; i++) {
            assertEquals(expectedVeggies[i].toString(), producedVeggies[i].toString());
        }
    }

    @Test
    public void testcreateClam() {
        EpicClam epicClam = new EpicClam();
        assertEquals(epicClam.getClass(), depokFactory.createClam().getClass());
    }
}