package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class EpicCheeseTest {

    private Class<?> epicCheeseClass;

    @Before
    public void setUp() throws Exception {
        epicCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.EpicCheese");
    }

    @Test
    public void testEpicCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(epicCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testEpicCheeseOverridetoStringMethod() throws Exception {
        Method toString = epicCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Cheese concreteEpicCheese = new EpicCheese();
        assertEquals("Epic Cheese", concreteEpicCheese.toString());
    }
}
