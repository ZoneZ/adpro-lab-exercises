package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class EpicDoughTest {

    private Class<?> epicDoughClass;

    @Before
    public void setUp() throws Exception {
        epicDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.EpicDough");
    }

    @Test
    public void testEpicDoughIsADough() {
        Collection<Type> classInterfaces = Arrays.asList(epicDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testEpicDoughOverridetoStringMethod() throws Exception {
        Method toString = epicDoughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Dough concreteEpicDough = new EpicDough();
        assertEquals("Epic dough", concreteEpicDough.toString());
    }
}
