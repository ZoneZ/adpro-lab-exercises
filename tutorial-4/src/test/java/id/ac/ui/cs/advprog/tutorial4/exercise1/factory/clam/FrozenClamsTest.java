package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class FrozenClamsTest {

    private Class<?> frozenClamClass;

    @Before
    public void setUp() throws Exception {
        frozenClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams");
    }

    @Test
    public void testFrozenClamsIsAClam() {
        Collection<Type> classInterfaces = Arrays.asList(frozenClamClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFrozenClamsOverridetoStringMethod() throws Exception {
        Method toString = frozenClamClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Clams concretefrozenClam = new FrozenClams();
        assertEquals("Frozen Clams from Chesapeake Bay", concretefrozenClam.toString());
    }
}
