package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class FreshClamsTest {

    private Class<?> freshClamClass;

    @Before
    public void setUp() throws Exception {
        freshClamClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.EpicClam");
    }

    @Test
    public void testFreshClamsIsAClam() {
        Collection<Type> classInterfaces = Arrays.asList(freshClamClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFreshClamsOverridetoStringMethod() throws Exception {
        Method toString = freshClamClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Clams concretefreshClam = new FreshClams();
        assertEquals("Fresh Clams from Long Island Sound", concretefreshClam.toString());
    }
}
