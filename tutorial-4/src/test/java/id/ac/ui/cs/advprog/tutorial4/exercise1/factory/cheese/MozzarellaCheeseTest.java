package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class MozzarellaCheeseTest {

    private Class<?> mozzarellaCheeseClass;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
    }

    @Test
    public void testMozzarellaCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(mozzarellaCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testMozzarellaCheeseOverridetoStringMethod() throws Exception {
        Method toString = mozzarellaCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Cheese concreteMozzarellaCheese = new MozzarellaCheese();
        assertEquals("Shredded Mozzarella", concreteMozzarellaCheese.toString());
    }
}