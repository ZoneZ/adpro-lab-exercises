package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class ParmesanCheeseTest {

    private Class<?> parmesanCheeseClass;

    @Before
    public void setUp() throws Exception {
        parmesanCheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese");
    }

    @Test
    public void testParmesanCheeseIsACheese() {
        Collection<Type> classInterfaces = Arrays.asList(parmesanCheeseClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testParmesanCheeseOverridetoStringMethod() throws Exception {
        Method toString = parmesanCheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Cheese concreteParmesanCheese = new ParmesanCheese();
        assertEquals("Shredded Parmesan", concreteParmesanCheese.toString());
    }
}