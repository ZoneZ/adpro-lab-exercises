package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class ThinCrustDoughTest {

    private Class<?> thinCrustDoughClass;

    @Before
    public void setUp() throws Exception {
        thinCrustDoughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough");
    }

    @Test
    public void testThinCrustDoughIsADough() {
        Collection<Type> classInterfaces = Arrays.asList(thinCrustDoughClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThinCrustDoughOverridetoStringMethod() throws Exception {
        Method toString = thinCrustDoughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Dough concreteThinCrustDough = new ThinCrustDough();
        assertEquals("Thin Crust Dough", concreteThinCrustDough.toString());
    }
}
