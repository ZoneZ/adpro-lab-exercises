package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private DepokPizzaStore depokStore;

    @Before
    public void setUp() throws Exception {
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void testDepokPizzaStoreCreateCheesePizza() {
        Pizza pizza = depokStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void testDepokPizzaStoreCreateVeggiePizza() {
        Pizza pizza = depokStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void testDepokPizzaStoreCreateClamPizza() {
        Pizza pizza = depokStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
