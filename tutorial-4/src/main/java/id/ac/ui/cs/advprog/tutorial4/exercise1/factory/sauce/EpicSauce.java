package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class EpicSauce implements Sauce {

    public String toString() {
        return "Epic sauce";
    }
}
