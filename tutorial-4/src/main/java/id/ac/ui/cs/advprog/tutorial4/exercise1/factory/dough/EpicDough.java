package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class EpicDough implements Dough {

    public String toString() {
        return "Epic dough";
    }
}
