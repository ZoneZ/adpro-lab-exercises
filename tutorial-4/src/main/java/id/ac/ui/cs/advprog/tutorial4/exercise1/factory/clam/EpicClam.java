package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class EpicClam implements Clams {

    public String toString() {
        return "Epic clam";
    }
}
