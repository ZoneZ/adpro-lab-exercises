package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class EpicCheese implements Cheese {

    public String toString() {
        return "Epic Cheese";
    }
}
