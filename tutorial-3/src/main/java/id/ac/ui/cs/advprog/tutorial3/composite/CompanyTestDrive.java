package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompanyTestDrive {

    public static void main(String[] args) {
        Company rockmoonGames = new Company();

        Employees ceo = new Ceo("Hesoyam", 200000000);
        Employees cto = new Cto("Aezakmi", 100000000);
        Employees backend = new BackendProgrammer("Jcnruad", 20000000);
        Employees frontend = new FrontendProgrammer("Uzumymw", 30000000);
        Employees networkExpert = new NetworkExpert("Buffmeup", 50000000);
        Employees securityExpert = new SecurityExpert("Baguvix", 70000000);
        Employees uiUxDesigner = new UiUxDesigner("goodbyecruelworld", 90000000);

        rockmoonGames.addEmployee(ceo);
        rockmoonGames.addEmployee(cto);
        rockmoonGames.addEmployee(backend);
        rockmoonGames.addEmployee(frontend);
        rockmoonGames.addEmployee(networkExpert);
        rockmoonGames.addEmployee(securityExpert);
        rockmoonGames.addEmployee(uiUxDesigner);

        System.out.println("Rockmoon Games team: ");
        for (Employees employee: rockmoonGames.getAllEmployees()) {
            System.out.format("%s : %s with salary %.3f\n",
                    employee.getRole(), employee.getName(), employee.getSalary());
        }

        System.out.println("The team's netted salary: " + rockmoonGames.getNetSalaries());
    }
}
