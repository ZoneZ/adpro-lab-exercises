package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String name, double salary) {
        this.name = name;
        this.setSalary(salary);
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
