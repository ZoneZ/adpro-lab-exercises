package id.ac.ui.cs.advprog.tutorial3.composite;

public abstract class Employees {
    protected String name = "Unidentified Name";
    protected double salary;
    protected String role;

    public String getName() {
        return this.name;
    }

    public abstract double getSalary();

    public String getRole() {
        return this.role;
    }

    public double getMinimumSalary() {
        double minimumSalary = 0;
        switch (this.getClass().getSimpleName()) {
            case "BackendProgrammer":
                minimumSalary = 20000000;
                break;
            case "Ceo":
                minimumSalary = 200000000;
                break;
            case "Cto":
                minimumSalary = 100000000;
                break;
            case "FrontendProgrammer":
                minimumSalary = 30000000;
                break;
            case "NetworkExpert":
                minimumSalary = 50000000;
                break;
            case "SecurityExpert":
                minimumSalary = 70000000;
                break;
            case "UiUxDesigner":
                minimumSalary = 90000000;
                break;
            default:
                minimumSalary = 0;
                break;
        }
        return minimumSalary;
    }

    public void setSalary(double salary) {
        double minimumSalary = this.getMinimumSalary();
        if (salary >= minimumSalary) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary must not be lower than "
                    + minimumSalary + "!");
        }
    }
}
