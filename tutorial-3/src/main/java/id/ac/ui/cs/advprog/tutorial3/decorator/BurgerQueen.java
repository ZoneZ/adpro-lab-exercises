package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class BurgerQueen {

    public static void main(String[] args) {
        Food simpleCrust = new CrustySandwich();
        System.out.println(simpleCrust.getDescription() + " $" + simpleCrust.cost());

        Food specialBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        specialBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.CHEESE.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.CUCUMBER.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.LETTUCE.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.CHILI_SAUCE.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.CHICKEN_MEAT.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(specialBurger);
        specialBurger = FillingDecorator.TOMATO.addFillingToBread(specialBurger);
        System.out.println(specialBurger.getDescription() + " $" + specialBurger.cost());
    }
}
