package id.ac.ui.cs.advprog.tutorial3.composite;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CompanyTest {
    private Company company;
    private Ceo luffy;
    private Cto zorro;
    private BackendProgrammer franky;
    private BackendProgrammer usopp;
    private FrontendProgrammer nami;
    private FrontendProgrammer robin;
    private UiUxDesigner sanji;
    private NetworkExpert brook;
    private SecurityExpert chopper;


    @Before
    public void setUp() {
        company = new Company();

        luffy = new Ceo("Luffy", 200000000.00);
        company.addEmployee(luffy);

        zorro = new Cto("Zorro", 100000000.00);
        company.addEmployee(zorro);

        franky = new BackendProgrammer("Franky", 20000000.00);
        company.addEmployee(franky);

        usopp = new BackendProgrammer("Usopp", 20000000.00);
        company.addEmployee(usopp);

        nami = new FrontendProgrammer("Nami",30000000.00);
        company.addEmployee(nami);

        robin = new FrontendProgrammer("Robin", 30000000.00);
        company.addEmployee(robin);

        sanji = new UiUxDesigner("sanji", 90000000.00);
        company.addEmployee(sanji);

        brook = new NetworkExpert("Brook", 50000000.00);
        company.addEmployee(brook);

        chopper = new SecurityExpert("Chopper", 70000000.00);
        company.addEmployee(chopper);
    }

    @Test
    public void addingSomeEmployees() {

        Employees[] arrayEmployeeComparation = {luffy, zorro, franky, usopp, nami,
                                                robin, sanji, brook, chopper};
        List<Employees> allEmployees = company.getAllEmployees();

        for (int index = 0; index < allEmployees.size(); index++) {
            assertEquals(arrayEmployeeComparation[index],allEmployees.get(index));
        }

    }

    @Test
    public void salaryTotalTest() {
        List<Employees> allEmployees = company.getAllEmployees();
        assertEquals(610000000.00, company.getNetSalaries(), 0.0001);
    }

}
